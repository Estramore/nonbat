#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>
#include <QMainWindow>
#include <QVector>
#include <QPointF>
#include <QPolygonF>

#include "marbleWindow.h"
#include <fileWorker.h>

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QMainWindow *parent = 0);
    ~MainWindow();

    QVector<QPointF> getArea();

private:
    void createFormInterior();
    void createToolbar();
    void createTrack();

    QVector<QPointF> m_areaPoints;
    QVector<QPointF> m_trackPoints;
    MarbleWindow *m_marbleWindow;
    FileWorker *m_fileWorker;

};

#endif // MAINWINDOW_H
