#ifndef FILEWORKER_H
#define FILEWORKER_H

#include <QtWidgets>
#include <QMainWindow>

class FileWorker: public QObject
{
    Q_OBJECT

public:
    FileWorker(QString filename, QWidget *parent);
    ~FileWorker();
    void setCurrentFile(const QString &fileName);
    void loadFile(const QString &fileName);

public slots:
//    void newFile();
    void open();
    bool save();
    bool saveAs();

private:
    bool maybeSave();
    bool saveFile(const QString &fileName);

    QString m_curFile;
    QPlainTextEdit *m_textEdit;
    QWidget *m_parent;
};

#endif // FILEWORKER_H
