#-------------------------------------------------
#
# Project created by QtCreator 2016-05-18T21:24:18
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = NoniusBati
TEMPLATE = app


SOURCES += main.cpp\
    mainWindow.cpp \
    marbleWindow.cpp \
    fileWorker.cpp \
    algorithm.cpp

HEADERS  += \
    mainWindow.h \
    marbleWindow.h \
    fileWorker.h \
    algorithm.h

LIBS += -lmarblewidget-qt5 \
    -L/usr/lib -lqwtplot3d-qt4 \
    -L/usr/lib/i386-linux-gnu -lGLU

INCLUDEPATH += /usr/include/qwtplot3d-qt4 \
    /usr/include/GL

RESOURCES += \
    source.qrc
