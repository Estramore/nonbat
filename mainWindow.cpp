#include "mainWindow.h"
#include "marbleWindow.h"
#include "fileWorker.h"
#include "algorithm.h"

#include <QtWidgets>


MainWindow::MainWindow(QMainWindow *parent) : QMainWindow(parent){
    setWindowTitle(tr("Something title"));
    setMinimumSize(1200, 600);

    m_fileWorker = new FileWorker("", this);

    createFormInterior();
    createToolbar();
}

MainWindow::~MainWindow(){

}

void MainWindow::createFormInterior(){
    QWidget * wgt = new QWidget(this);
    setCentralWidget(wgt);

    QHBoxLayout * boxLayout = new QHBoxLayout(wgt);
    QTabWidget *tabWidget = new QTabWidget(wgt);

    m_marbleWindow = new MarbleWindow(wgt);
    m_marbleWindow->setData(m_areaPoints, m_trackPoints);

    tabWidget->addTab(m_marbleWindow, tr("Map"));

    boxLayout->addWidget(tabWidget);
}

void MainWindow::createTrack(){
    m_marbleWindow->getAreaPoints();
}

QVector<QPointF> MainWindow::getArea(){
    return m_areaPoints;
}

void MainWindow::createToolbar(){
    QToolBar *topToolBar = new QToolBar(this);
    addToolBar(Qt::TopToolBarArea, topToolBar);
    topToolBar->setMovable(false);
//    topToolBar->setStyleSheet("QToolBar {background: rgb(107, 160, 188)}");
//    topToolBar->setStyleSheet("QToolBar {background: rgb(126, 183, 212)}");

    //LOAD
    topToolBar->addAction(QIcon(":/new/prefix1/icons/load.png"), tr("Открыть файл"), m_fileWorker, SLOT(open()));

    //SAVE
    topToolBar->addAction(QIcon(":/new/prefix1/icons/save.png"), tr("Сохранить в файл"), m_fileWorker, SLOT(save()));
    topToolBar->addSeparator();

    //HAND
    topToolBar->addAction(QIcon(":/new/prefix1/icons/hand.png"), tr("Hand"), m_marbleWindow, SLOT(toolSelected()))->setCheckable(true);
    topToolBar->actions().last()->setObjectName("hand");
    topToolBar->addSeparator();

    //AREA
    topToolBar->addAction(QIcon(":/new/prefix1/icons/area.png"), tr("Выбрать область"), m_marbleWindow, SLOT(toolSelected()))->setCheckable(true);
    topToolBar->actions().last()->setObjectName("area");

    //Erase AREA
    topToolBar->addAction(QIcon(":/new/prefix1/icons/eraser.png"), tr("Очистить область"), m_marbleWindow, SLOT(eraseArea()));
    topToolBar->actions().last()->setObjectName("area");
    topToolBar->addSeparator();

    //TRACK
//    topToolBar->addAction(QIcon(":/new/prefix1/icons/track.png"), tr("Построить маршрут"), m_marbleWindow, SLOT(createThrack()));
//    topToolBar->actions().last()->setObjectName("track");

    QMenu *menu = new QMenu();
    menu->addAction(tr("Вручную"), m_marbleWindow, SLOT(toolSelected()))->setCheckable(true);
    menu->actions().last()->setObjectName("trackByHand");

    QToolButton* toolButton = new QToolButton();
    toolButton->setMenu(menu);
    toolButton->setIcon(QIcon(":/new/prefix1/icons/track.png"));
    toolButton->setPopupMode(QToolButton::InstantPopup);
    topToolBar->addWidget(toolButton);
    topToolBar->addSeparator();


    //PLAY
    topToolBar->addAction(QIcon(":/new/prefix1/icons/play.png"), tr("Поехали"), m_marbleWindow, SLOT());
    topToolBar->actions().last()->setObjectName("track");

    //STOP
    topToolBar->addAction(QIcon(":/new/prefix1/icons/stop.png"), tr("Возвратиться на базу"), m_marbleWindow, SLOT());
    topToolBar->actions().last()->setObjectName("track");

    //PAUSE
//    m_topToolBar->addAction(QIcon(":/new/prefix1/icons/pause.png"), tr("Остановиться"), m_marbleWindow, SLOT());
//    m_topToolBar->actions().last()->setObjectName("track");
    topToolBar->addSeparator();

}

