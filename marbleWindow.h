#ifndef MARBLEWINDOW_H
#define MARBLEWINDOW_H

#include <QWidget>
#include <marble/MarbleWidget.h>
#include <marble/GeoDataLinearRing.h>
#include <marble/GeoDataStyle.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataDocument.h>
#include <QPointF>
#include <QVector>



using namespace Marble;

class MarbleWindow : public MarbleWidget{
    Q_OBJECT
public:
    explicit MarbleWindow(QWidget  *parent = 0);
    ~MarbleWindow();
    virtual void customPaint(Marble::GeoPainter *painter);
    QVector<QPointF> getAreaPoints();
    void setData(QVector<QPointF> &vectArea, QVector<QPointF> &vectTrack);

public slots:
    void toolSelected();
    void eraseArea();
    void createTrack();

protected:
    virtual void mousePressEvent(QMouseEvent *event);
//    virtual void mouseMoveEvent(QMouseEvent *event);
//    virtual void mouseReleaseEvent(QMouseEvent *event);

private:

    void buildRegion(GeoDataLineString &lineString, QVector<QPointF> &vrector);
    void drawingEnded(bool save);
    void addDrawable(GeoDataGeometry &drawable);
    void selectTool(QString toolName);

    QSet<Marble::GeoDataGeometry *> drawables;
    GeoDataDocument *doc;
    GeoDataDocument *mapToolsDoc;

    QString m_currentTool;
    QAction *m_tool;
    QVector<QPointF> m_vect;
    QVector<QPointF> m_track;
};





#endif // MARBLEWINDOW_H
