#include "fileWorker.h"

#include <QtWidgets>

FileWorker::FileWorker(QString filename, QWidget *parent){
    m_curFile = filename;
    m_parent = parent;
}

FileWorker::~FileWorker(){

}

void FileWorker::open(){
    QString fileName = QFileDialog::getOpenFileName(m_parent);
    if (!fileName.isEmpty())
        loadFile(fileName);
}

void FileWorker::loadFile(const QString &fileName){
    QFile file(fileName);
    if (!file.open(QFile::ReadOnly | QFile::Text)) {
        QMessageBox::warning(m_parent, tr("Application"),
                             tr("Cannot read file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName), file.errorString()));
        return;
    }

    setCurrentFile(fileName);
//    statusBar()->showMessage(tr("File loaded"), 2000);
}

bool FileWorker::save(){
    if (m_curFile.isEmpty()) {
        return saveAs();
    }
    else {
        return saveFile(m_curFile);
    }
}

bool FileWorker::saveFile(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QFile::WriteOnly | QFile::Text)) {
        QMessageBox::warning(m_parent, tr("Application"),
                             tr("Cannot write file %1:\n%2.")
                             .arg(QDir::toNativeSeparators(fileName),
                                  file.errorString()));
        return false;
    }

    QTextStream out(&file);
#ifndef QT_NO_CURSOR
    QApplication::setOverrideCursor(Qt::WaitCursor);
#endif
    out << m_textEdit->toPlainText();
#ifndef QT_NO_CURSOR
    QApplication::restoreOverrideCursor();
#endif

    setCurrentFile(fileName);
//    statusBar()->showMessage(tr("File saved"), 2000);
    return true;
}

bool FileWorker::saveAs(){
    QFileDialog dialog(m_parent);
    dialog.setWindowModality(Qt::WindowModal);
    dialog.setAcceptMode(QFileDialog::AcceptSave);

    if (dialog.exec() != QDialog::Accepted)
        return false;

    return saveFile(dialog.selectedFiles().first());
}

void FileWorker::setCurrentFile(const QString &fileName){
    m_curFile = fileName;
    QString shownName = m_curFile;
//    if (m_curFile.isEmpty())
//        shownName = "untitled.txt";
//    m_parent->setWindowFilePath(shownName);
}
