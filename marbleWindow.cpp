#include "marbleWindow.h"
#include "mainWindow.h"

#include <QPen>
#include <QColor>
#include <QMouseEvent>
#include <QPointF>
#include <QAction>
#include <QPolygonF>


#include <marble/MarbleWidget.h>
#include <marble/MarbleModel.h>
#include <marble/GeoDataDocument.h>
#include <marble/GeoDataCoordinates.h>
#include <marble/GeoDataPlacemark.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoDataLinearRing.h>
#include <marble/GeoDataTreeModel.h>
#include <marble/GeoDataStyle.h>
#include <marble/GeoDataIconStyle.h>
#include <marble/GeoDataLineStyle.h>
#include <marble/GeoDataPolyStyle.h>
#include <marble/GeoDataLineString.h>
#include <marble/GeoPainter.h>
#include <marble/MarbleWidgetInputHandler.h>


using namespace Marble;

MarbleWindow::MarbleWindow(QWidget *parent): MarbleWidget(parent) {

    // Load the Plain map
    this->setMapThemeId("earth/openstreetmap/openstreetmap.dgml");
    this->zoomView( 2800 );
    this->setShowCompass(false);
    this->setShowOverviewMap(false);

    // Center the map onto a given position
    GeoDataCoordinates home(30.083333333, 59.866666667, 0.0, GeoDataCoordinates::Degree);
    this->centerOn(home);

    doc = new Marble::GeoDataDocument();
    MarbleWidget::model()->treeModel()->addDocument(doc);
    mapToolsDoc = new Marble::GeoDataDocument();
    MarbleWidget::model()->treeModel()->addDocument(mapToolsDoc);

    MarbleWidget::setMapQualityForViewContext(Marble::PrintQuality, Marble::Still);
    inputHandler()->setMouseButtonPopupEnabled(Qt::LeftButton, false);
    inputHandler()->setMouseButtonPopupEnabled(Qt::RightButton, false);
    inputHandler()->setInertialEarthRotationEnabled(false);
    this->setInputEnabled(false);
//    this->setCursor(QCursor::);
    this->setDefaultAngleUnit(Marble::DecimalDegree);
}

void MarbleWindow::createTrack(){
    QPolygonF poly(m_track);

}

void MarbleWindow::buildRegion(GeoDataLineString &lineString, QVector<QPointF> &vrector){
    lineString.clear();
    foreach (QPointF va_arg, vrector) {
        lineString << GeoDataCoordinates(va_arg.x(), va_arg.y(), 0, GeoDataCoordinates::Degree );
    }
}

void MarbleWindow::customPaint(GeoPainter* painter) {

    GeoDataLineString areaLine( NoTessellation );
    buildRegion(areaLine, m_vect);

    GeoDataLineString trackLine( NoTessellation );
    buildRegion(trackLine, m_track);

    painter->setPen(QPen(QBrush(QColor::NameFormat(000000)), 3.0));//, Qt::SolidLine, Qt::RoundCap ));
    painter->drawPolyline(areaLine);

//    pen.setColor(QColor::NameFormat(0xAEF100));
    painter->setPen(QPen(QBrush(QColor::NameFormat(0xAEF100)), 2.0));//, Qt::SolidLine, Qt::RoundCap ));
    painter->drawPolyline(trackLine);
}

void MarbleWindow::mousePressEvent(QMouseEvent *event) {
    if(!m_currentTool.isNull()){
        if(event->button() == Qt::LeftButton){
            if(m_tool->isChecked() && m_currentTool == "trackByHand"){
                qreal lon, lat;
                geoCoordinates(event->x(), event->y(), lon, lat);
                m_track.push_back(QPointF(lon, lat));
            }
            if(m_tool->isChecked() && m_currentTool == "area"){
                qreal lon, lat;
                geoCoordinates(event->x(), event->y(), lon, lat);
                m_vect.push_back(QPointF(lon, lat));
            }
            this->update();
        }
        else if (event->button() == Qt::RightButton){
            if (m_tool->isChecked()){
                if(m_currentTool == "area" && m_vect.size()>2){
                    m_vect.push_back(m_vect.first());
                    this->update();
                }
                if(m_currentTool == "trackByHand"){
                    this->update();
                }
                m_currentTool.clear();
                this->setInputEnabled(false);
                m_tool->setChecked(false);
            }
        }
    }
}

void MarbleWindow::eraseArea(){
    m_vect.clear();
    m_track.clear();
    this->update();
}

void MarbleWindow::toolSelected(){
    m_tool = (QAction *)this->sender();
    m_currentTool = m_tool->objectName();

    if (m_currentTool == "trackByHand" && m_tool->isChecked()){
        if (m_vect.isEmpty()){
            QErrorMessage errorMessage;
            errorMessage.showMessage("Сначала посторой область");
            errorMessage.exec();
            m_tool->setChecked(false);
            return;
        }
        this->setCursor(Qt::CrossCursor);
        m_track.clear();

    }
    if (m_currentTool == "area" && m_tool->isChecked()){
        this->setCursor(Qt::CrossCursor);
    }
    if (m_currentTool == "hand"){
        this->setInputEnabled(m_tool->isChecked());
    }
}

void MarbleWindow::selectTool(QString toolName){
    m_currentTool = toolName;
}

void MarbleWindow::setData(QVector<QPointF> &vectArea, QVector<QPointF> &vectTrack){
    m_vect = vectArea;
    m_track = vectTrack;
}

QVector<QPointF> MarbleWindow::getAreaPoints(){
    return m_vect;
}

MarbleWindow::~MarbleWindow(){

}
