#ifndef ALGORITHM_H
#define ALGORITHM_H

#include <QObject>
#include <QVector>
#include <QVector2D>

class Algorithm : public QObject
{
    Q_OBJECT
public:
    explicit Algorithm(QObject *parent = 0);
    void createTrack(QVector<QPointF> area);

signals:

public slots:
};

#endif // ALGORITHM_H
